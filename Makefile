ifeq ($(shell test -e '.env' && echo -n yes),yes)
	include .env
endif

# parse additional args for commands

args := $(wordlist 2, 100, $(MAKECMDGOALS))
ifndef args
MESSAGE = "No such command (or you pass two or many targets to ). List of possible commands: make help"
else
MESSAGE = "Done"
endif

APPLICATION_NAME = app
CODE = $(APPLICATION_NAME)

HELP_FUN = \
	%help; while(<>){push@{$$help{$$2//'options'}},[$$1,$$3] \
	if/^([\w-_]+)\s*:.*\#\#(?:@(\w+))?\s(.*)$$/}; \
    print"$$_:\n", map"  $$_->[0]".(" "x(20-length($$_->[0])))."$$_->[1]\n",\
    @{$$help{$$_}},"\n" for keys %help; \


# Commands
env:  ##@Environment Create .env file with variables
	@$(eval SHELL:=/bin/bash)
	@cp .env.example .env

help: ##@Help Show this help
	@echo -e "Usage: make [target] ...\n"
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

#db:  ##@Database Create database with docker-compose
#	docker-compose -f docker-compose.yaml up -d --remove-orphans

lint:  ##@Code Check code with pylint
	poetry run python -m pylint $(CODE)

format:  ##@Code Reformat code with isort and black
	poetry run python -m isort $(CODE)
	poetry run python -m black $(CODE)

run:  ##@Application Run application server
	poetry run python -m $(APPLICATION_NAME)

fill.datadb:  ##@fill data in db
	docker exec -it mongodb sh -c "mongorestore --authenticationDatabase=admin --uri mongodb://$(MONGODB_USER):$(MONGODB_PASSWORD)@$(MONGODB_HOST):$(MONGODB_PORT) /docker-entrypoint-initdb.d/ -d sampleDB"
