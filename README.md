Создание виртуального окружения и установка зависимостей:  
>       poetry install  
>       poetry shell  

Создание бд:  
>       sudo docker-compose up -d  

Загрузка дампа в бд:
>       sudo make fill.datadb  
 
Запуск приложения:
>       make run  

Структура проекта:  
+   app - код приложения,  
+   config - настройки приложения,  
+   utils - работа с бд,        
+   .env - файл с переменными окружения,    

Ник бота: @Test_task353_bot
