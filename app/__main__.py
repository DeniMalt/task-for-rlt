import datetime
import json
import sys
import logging
import time

from motor.motor_asyncio import AsyncIOMotorClient
from aiogram import Bot, types, Dispatcher, executor

from app.config import get_settings
from app.utils import aggregator


settings = get_settings()

bot = Bot(token=settings.TOKEN)
dp = Dispatcher(bot=bot)
client = AsyncIOMotorClient(settings.database_uri)


@dp.message_handler(commands=['start'])
async def greeting(message: types.Message):
    user_id = message.from_user.id
    user_full_name = message.from_user.full_name
    logging.info(f'{user_id} {user_full_name} {time.asctime()} response: 200 message: ok')
    await message.answer(f"Hi {user_full_name}!")


@dp.message_handler()
async def aggregation(message: types.Message):
    try:
        mes = json.loads(message.text)
        await message.answer(
            await aggregator(
                    client=client,
                    dt_from=datetime.datetime.strptime(mes["dt_from"], '%Y-%m-%dT%H:%M:%S'),
                    dt_upto=datetime.datetime.strptime(mes["dt_upto"], '%Y-%m-%dT%H:%M:%S'),
                    group_type=mes["group_type"]
            )
        )
        logging.info(f'{message.from_user.id} '
                     f'{message.from_user.full_name} {time.asctime()} response: 200 message: ok')
    except (json.JSONDecodeError, KeyError):
        await message.answer(
            """Невалидный запрос. Пример запроса:\n{"dt_from":"2022-09-01T00:00:00","dt_upto": "2022-12-31T23:59:00", "group_type": "month"}"""
        )
        logging.info(f'{message.from_user.id} {message.from_user.full_name}'
                     f' {time.asctime()} response: 400 message: invalid data')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    executor.start_polling(dp)
