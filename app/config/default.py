from os import environ

from pydantic import BaseSettings


class DefaultSettings(BaseSettings):
    """
    Default configs for application.

    Usually, we have three environments: for development, testing and production.
    But in this situation, we only have standard settings for local development.
    """

    ENV: str = environ.get("ENV", "local")
    MONGODB_DB: str = environ.get("MONGODB_DB", "db")
    MONGODB_PASSWORD: str = environ.get("MONGODB_PASSWORD", "pass")
    MONGODB_USER: str = environ.get("MONGODB_USER", "admin")
    MONGODB_PORT: int = int(environ.get("MONGODB_PORT", "27017"))
    MONGODB_HOST: str = environ.get("MONGODB_HOST", "localhost")
    TOKEN: str = environ.get("TOKEN", "6563159192:AAF8zQjmdR2HtV1ZSklWoJDkef9M3i49ZKc")

    @property
    def database_settings(self) -> dict:
        """
        Get all settings for connection with database.
        """
        return {
            "database": self.MONGODB_DB,
            "user": self.MONGODB_USER,
            "password": self.MONGODB_PASSWORD,
            "host": self.MONGODB_HOST,
            "port": self.MONGODB_PORT,
        }

    @property
    def database_uri(self) -> str:
        """
        Get uri for connection with database.
        """
        return "mongodb://{user}:{password}@{host}:{port}".format(
            **self.database_settings,
        )

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
