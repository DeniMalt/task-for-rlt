import datetime
import json

from dateutil.relativedelta import relativedelta

from motor.motor_asyncio import AsyncIOMotorClient


async def group_data(ans, docs):
    values_interval = []
    for ind_l in range(1, len(ans["labels"])):
        for ind_d in range(len(docs)):
            if ans["labels"][ind_l - 1] <= docs[ind_d]['dt'] < ans["labels"][ind_l]:
                values_interval.append(docs[ind_d]['value'])
        ans['dataset'].append(sum(values_interval))
        values_interval.clear()
    for ind_l in range(len(ans['labels'])):
        ans['labels'][ind_l] = datetime.datetime.strftime(ans['labels'][ind_l], '%Y-%m-%dT%H:%M:%S')
    ans['labels'] = ans['labels'][:-1]
    return ans


async def aggregator(client: AsyncIOMotorClient,
                     dt_from: datetime.datetime,
                     dt_upto: datetime.datetime,
                     group_type: str):
    db = client.sampleDB

    ins = db.sample_collection.find(
        {
            'dt':
                {
                    '$gte': dt_from,
                    '$lte': dt_upto
                }
        }
    )

    docs = []
    async for doc in ins:
        docs.append(doc)

    ans = {"dataset": [], "labels": [dt_from]}

    if group_type == "hour":

        ans["labels"].append(dt_from + datetime.timedelta(hours=1))

        while ans["labels"][-1] <= dt_upto:
            ans["labels"].append(ans["labels"][-1] + datetime.timedelta(hours=1))

    if group_type == "month":
        ans["labels"].append(dt_from + relativedelta(months=1))

        while ans["labels"][-1] <= dt_upto:
            ans["labels"].append(ans["labels"][-1] + relativedelta(months=1))

    if group_type == "day":
        ans["labels"].append(dt_from + datetime.timedelta(days=1))

        while ans["labels"][-1] <= dt_upto:
            ans["labels"].append(ans["labels"][-1] + datetime.timedelta(days=1))

    return json.dumps(await group_data(ans, docs))
